package com.solvative.practicaltask.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.solvative.practicaltask.Models.Person;
import com.solvative.practicaltask.Repository.PersonRepository;

public class PersonViewModel extends ViewModel {

    private LiveData<Person> personLiveData;

    public PersonViewModel() {
        super();
        PersonRepository personRepository = new PersonRepository();
        personLiveData = personRepository.getPersonsData();
    }

    public LiveData<Person> getPersonLiveData() {
        return personLiveData;
    }
}