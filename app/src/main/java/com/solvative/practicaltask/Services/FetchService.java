package com.solvative.practicaltask.Services;

import com.solvative.practicaltask.Models.Person;

import retrofit2.Call;
import retrofit2.http.GET;

public interface FetchService {

    @GET("/api/users")
    Call<Person> getPersons();

}
