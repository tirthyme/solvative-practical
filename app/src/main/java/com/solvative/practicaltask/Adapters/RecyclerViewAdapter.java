package com.solvative.practicaltask.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.solvative.practicaltask.Models.Data;
import com.solvative.practicaltask.R;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Data> personArrayList;

    public RecyclerViewAdapter(Context context, ArrayList<Data> personArrayList) {
        this.context = context;
        this.personArrayList = personArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(String.format("%s %s", personArrayList.get(position).getFirst_name(), personArrayList.get(position).getLast_name()));
        holder.email.setText(personArrayList.get(position).getEmail());
        Glide.with(context).load(personArrayList.get(position).getAvatar()).into(holder.pfp);
    }

    @Override
    public int getItemCount() {
        return personArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, email;
        private ImageView pfp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            email = itemView.findViewById(R.id.email);
            pfp = itemView.findViewById(R.id.profile_picture);
        }
    }
}
