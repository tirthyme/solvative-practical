package com.solvative.practicaltask;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.solvative.practicaltask.Adapters.RecyclerViewAdapter;
import com.solvative.practicaltask.Models.Data;
import com.solvative.practicaltask.Models.Person;
import com.solvative.practicaltask.ViewModel.PersonViewModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Data> personsData = new ArrayList<>();
    PersonViewModel viewModel;
    RecyclerViewAdapter recyclerViewAdapter;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.progress_circular);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerViewAdapter = new RecyclerViewAdapter(this, personsData);
        recyclerView.setAdapter(recyclerViewAdapter);

        viewModel = new ViewModelProvider(MainActivity.this).get(PersonViewModel.class);

        viewModel.getPersonLiveData().observe(this, new Observer<Person>() {
            @Override
            public void onChanged(Person person) {
                if (person != null) {
                    personsData.addAll(person.getData());
                    recyclerViewAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(MainActivity.this, "Data Couldn't be fetched", Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}