package com.solvative.practicaltask.Repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.solvative.practicaltask.Models.Person;
import com.solvative.practicaltask.Network.Instance;
import com.solvative.practicaltask.Services.FetchService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonRepository {

    private static final String TAG = "PersonRepository";

    private FetchService fetchService;
//    private Retrofit retrofit;

    public PersonRepository() {
        fetchService = Instance.getRetrofit().create(FetchService.class);
    }

    public LiveData<Person> getPersonsData() {
        final MutableLiveData<Person> personMutableLiveData = new MutableLiveData<>();
        fetchService.getPersons().enqueue(new Callback<Person>() {
            @Override
            public void onResponse(Call<Person> call, Response<Person> response) {
                if (response.isSuccessful()) {
                    personMutableLiveData.setValue(response.body());
                } else {
                    personMutableLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<Person> call, Throwable t) {
                personMutableLiveData.setValue(null);
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
        return personMutableLiveData;
    }

}
